﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManagementSystem.Tables;
using TaskManagementSystem.Services;

namespace TaskManagementSystem.Services
{
    public class TaskServices : ITaskServices
    {
        protected DataContext db;
        public TaskServices()
        {
            db = new DataContext();
        }

        public bool AddTask(TaskViewModel Model)
        {
            var data = new Task()
            {
                Name = Model.Name,
                SkillId = Model.Id,
                Description = Model.Description,
                AddedDate = System.DateTime.Now,
                EndDate = Model.EndDate,
                Status = Model.Status
            };
            db.Tasks.Add(data);
            db.SaveChanges();
            return true;
        }

        public bool DeleteTask(int Id)
        {
            var IsExistData = db.Tasks.Where(x => x.Id == Id).FirstOrDefault();
            db.Tasks.Remove(IsExistData);
            db.SaveChanges();
            return true;
        }

        public IEnumerable<TaskViewModel> GetTask()
        {
            //var query = db.Tasks.Select(x => new TaskViewModel()
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    Description = x.Description,
            //    AddedDate = x.AddedDate,
            //    EndDate = x.EndDate,
            //    Status = x.Status
            //}).ToList();

            var query = (from t in db.Tasks
                         join s in db.Skills on t.SkillId equals s.Id
                         select new TaskViewModel()
                         {
                             Id = t.Id,
                             Name = t.Name,
                             SkillId = s.Id,
                             SkillName = s.SkillName,
                             Description = t.Description,
                             AddedDate = t.AddedDate,
                             EndDate = t.EndDate,
                             Status = t.Status
                         }).ToList();

            return query;
        }

        public TaskViewModel GetTask(int id)
        {
            var query = db.Tasks.Where(x => x.Id == id).Select(x => new TaskViewModel
            {
                Id = x.Id,
                Name = x.Name,
                SkillId = x.SkillId,
                Description = x.Description,
                AddedDate = x.AddedDate,
                EndDate = x.EndDate,
                Status = x.Status
            }).FirstOrDefault();

            return query;
        }
        public bool UpdateTask(TaskViewModel Model)
        {
            var data = new TaskViewModel();

            if (data != null)
            {
                var isExist = db.Tasks.Where(x => x.Id == Model.Id).FirstOrDefault();
                if (isExist != null)
                {
                    isExist.Name = Model.Name;
                    isExist.SkillId = Model.SkillId;
                    isExist.Description = Model.Description;
                    isExist.EndDate = Model.EndDate;
                    isExist.Status = Model.Status;
                }
                db.SaveChanges();
            }
            return true;
        }
    }
}