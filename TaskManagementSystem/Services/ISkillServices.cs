﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Services
{
    public interface ISkillServices
    {
        bool AddSkill(SkillViewModel Model);
        IEnumerable<SkillViewModel> GetSkill();
        bool UpdateSkill(SkillViewModel Model);
        //bool DeleteSkill(int Id);
        SkillViewModel GetSkill(int id);
        
    }
}