﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Services
{
    public class SkillServices : ISkillServices
    {
        protected DataContext db;
        public SkillServices()
        {
            db = new DataContext();
             
        }
        public bool AddSkill(SkillViewModel Model)
        {
            var data = new Skill()
            {
                SkillName = Model.SkillName,
     
            };
            db.Skills.Add(data);
            db.SaveChanges();
            return true;
        }

        //public bool DeleteSkill(int Id)
        //{
        //    var IsExistData = db.Skills.Where(x => x.Id == Id).FirstOrDefault();
        //    db.Skills.Remove(IsExistData);
        //    db.SaveChanges();
        //    return true;

        //}

        public IEnumerable<SkillViewModel> GetSkill()
        {
            var query = (from s in db.Skills
                        join e in db.Employees on s.Id equals e.SkillId into newvar   
                        select new SkillViewModel()
                         {
                             Id = s.Id,
                             SkillName = s.SkillName,
                             NoOfEmp = newvar.Count()
                         }).ToList();
           
          
            return query;
        }

        public SkillViewModel GetSkill(int id)
        {
            var query = db.Skills.Where(x => x.Id == id).Select(x => new SkillViewModel
            {
                Id = x.Id,
                SkillName = x.SkillName
            }).FirstOrDefault();

            return query;
        }

         public bool UpdateSkill(SkillViewModel Model)
    {
        var data = new SkillViewModel();

        if (data != null)
        {
            var isExist = db.Skills.Where(x => x.Id == Model.Id).FirstOrDefault();
            if (isExist != null)
            {
                isExist.SkillName = Model.SkillName;
            }
            db.SaveChanges();
        }
        return true;
    }

      
    }
}