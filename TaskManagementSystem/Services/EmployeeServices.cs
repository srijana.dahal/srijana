﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Services
{
    public class EmployeeServices : IEmployeeServices
    {
        protected DataContext db;
        public EmployeeServices()
        {
            db = new DataContext();
        }
        public bool AddEmployee(EmployeeViewModel Model)
        {
            var data = new Employee()
            {
                FirstName = Model.FirstName,
                LastName = Model.LastName,
                Email = Model.Email,
                SkillId = Model.Id,
                OtherSkillName = Model.OtherSkillName,
                Address = Model.Address,
                PhoneNumber = Model.PhoneNumber,
                AddedDate = System.DateTime.Now,
                Status = Model.Status
            };
            db.Employees.Add(data);
            db.SaveChanges();
            return true;
        }

        public bool DeleteEmployee(string Id)
        {
            //int[] arrayId; 
            var arrayId = Id.Split(',');
            int[] intarray = new int[arrayId.Length];
            for (int i = 0; i < arrayId.Length; i++)
            {
                intarray[i] = int.Parse(arrayId[i]);
            }
            for (int i = 0; i < intarray.Length; i++)
            {
                int EmpId = intarray[i];
                //var IsExistData = db.Employees.Where(x => x.Id == intarray[i]).FirstOrDefault();
                var IsExistData = (from e in db.Employees where e.Id == EmpId select e).FirstOrDefault();
                db.Employees.Remove(IsExistData);
                db.SaveChanges();
              
              }
            return true;
        }

        public IEnumerable<EmployeeViewModel> GetEmployee()
        {
            var query = (from e in db.Employees
                         join s in db.Skills on e.SkillId equals s.Id
                         select new EmployeeViewModel()
                         {
                            Id = e.Id,
                            FirstName = e.FirstName,
                            LastName = e.LastName,
                            Email = e.Email,
                            SkillId = s.Id,
                            SkillName = s.SkillName,
                            OtherSkillName = e.OtherSkillName,
                            Address = e.Address,
                            PhoneNumber = e.PhoneNumber,
                            AddedDate = System.DateTime.Now,
                            Status = e.Status
                        }).ToList();
          return query;
        }

        public EmployeeViewModel GetEmployee(int id)
        {
            var query = db.Employees.Where(x => x.Id == id).Select(x => new EmployeeViewModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                SkillId = x.SkillId,
                OtherSkillName = x.OtherSkillName,
                Address = x.Address,
                PhoneNumber = x.PhoneNumber,
                AddedDate = System.DateTime.Now,
                Status = x.Status
            }).FirstOrDefault();

            return query;
        }

        public bool UpdateEmployee(EmployeeViewModel Model)
        {

            var data = new EmployeeViewModel();

            if (data != null)
            {
                var isExist = db.Employees.Where(x => x.Id == Model.Id).FirstOrDefault();
                if (isExist != null)
                {
                    isExist.FirstName = Model.FirstName;
                    isExist.LastName = Model.LastName;
                    isExist.Email = Model.Email;
                    isExist.SkillId = Model.SkillId;
                    isExist.OtherSkillName = Model.OtherSkillName;
                    isExist.Address = Model.Address;
                    isExist.PhoneNumber = Model.PhoneNumber;
                    isExist.Status = Model.Status;
                }
                db.SaveChanges();
            }
            return true;
        }

    }
}