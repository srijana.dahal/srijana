﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Services
{
    public interface IEmployeeServices
    {
        bool AddEmployee(EmployeeViewModel Model);
        IEnumerable<EmployeeViewModel> GetEmployee();
        bool UpdateEmployee(EmployeeViewModel Model);
        bool DeleteEmployee(string Id);
        EmployeeViewModel GetEmployee(int id);

    }
}
