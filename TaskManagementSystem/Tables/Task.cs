﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskManagementSystem.Tables
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SkillId { get; set; }
        public string Description { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Status { get; set; }
    }

    public class TaskViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public string Description { get; set; }
        public DateTime AddedDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public bool Status { get; set; }
        
    }
   
}