﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TaskManagementSystem.Tables
{
    public class DataContext : DbContext
    {
        public DataContext() : base("connString")
        {
        }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Employee> Employees { get; set; }
       
    }
}