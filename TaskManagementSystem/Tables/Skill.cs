﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementSystem.Tables
{
    public class Skill
    {
        public int Id { get; set; }
        public string SkillName { get; set; }
    }

    public class SkillViewModel
    {
        public int Id { get; set; }
        public string SkillName { get; set; }
        public int? NoOfEmp { get; set; }
    }

}