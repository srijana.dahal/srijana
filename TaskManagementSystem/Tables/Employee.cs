﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace TaskManagementSystem.Tables
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int SkillId { get; set; }
        public string OtherSkillName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Status { get; set; }
    }

    public class EmployeeViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage ="First Name Required")]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public string OtherSkillName { get; set; }
        public string Address { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
         public string PhoneNumber { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Status { get; set; }
        public int NoOfEmp { get; internal set; }
    }
}