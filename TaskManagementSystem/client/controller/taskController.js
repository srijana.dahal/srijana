﻿(function () {
    'use strict';

    myApp.controller('taskController', taskController);

    taskController.$inject = ['$scope', '$location', 'taskServices', 'skillServices', '$filter'];

    function taskController($scope, $location, taskServices, skillServices, $filter) {

        $scope.CurrentPage = 1;
        $scope.itemsPerPage = 5;
        function GetSkill() {
            skillServices.GetSkill().then(function (data) {
                $scope.GetSkill = data;
            })
        };
        GetSkill();

        GetTask();
        $scope.Task = {};
        $scope.CreateTask = {};

        function GetTask() {
            taskServices.GetTask().then(function (data) {
                $scope.Tasks = data;  //data is passed to UI

            })
        };

        GetTask();
        $scope.GetById = function (id) {
            taskServices.GetById(id).then(function (data) {
                if (data != null) {
                    $scope.Task = data;
                }
            })
        };
        $scope.AddTask = function (model) {
            taskServices.AddTask(model).then(function (data) {  //addTask is passed to taskServices.
                if (data) {
                    $.notify({
                        title: "",
                        message: "Task Successfully Added!",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });                  
                    GetTask();                    
                    $("#myModal").modal('hide');
                }

            })
        };

        $scope.UpdateTask = function (model) {
            taskServices.UpdateTask(model).then(function (data) {
                if (data) {
                    $.notify({
                        title: "",
                        message: "Task Successfully Updated",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
                    GetTask();
                    $("#myModal1").modal('hide');
                }
            })
        };

        $scope.DeleteTask = function (id) {
            var result = confirm("Are you sure want to delete?");
            if (result) {
                taskServices.DeleteTask(id).then(function (data) {
                    if (data) {
                        $.notify({
                            title: "",
                            message: "Task Successfully Updated",
                            icon: 'fa fa-check'
                        }, {
                            type: "info"
                        });
                        GetTask();
                        $("#modal2").modal('hide');
                    }
                })
            }

        };
        //search function
        

    }
})();