﻿(function () {
    'use strict';

    myApp.controller('employeeCtrl', employeeCtrl);

    employeeCtrl.$inject = ['$scope', '$location', 'employeeServices', 'skillServices', '$filter'];

    function employeeCtrl($scope, $location, employeeServices, skillServices, $filter) {


       //for checkbox
        var executed = false;
        $scope.selectall = false;
        $scope.selectcheckbox=  function (selectall) {
            if (selectall) {
                $scope.Employees.forEach(function (data) {
                    data.selected = true;
                })
            } else {
                $scope.Employees.forEach(function (data) {
                    data.selected = false;
                })
            }
        };

        $scope.checkuncheck = function () {
       
        //var uncheckedDataList = jQuery.grep($scope.Employees, function (employee, i) {
         //   return employee.selected == false;
        //});
            var checkedDataList = jQuery.grep($scope.Employees, function (employee, i) {
                return employee.selected == true;
            });
            var check = $scope.Employees.length;
        if (checkedDataList.length != check) {
            $scope.selectall = false;
        }
        else {
            $scope.selectall = true;
        }
        };

        $scope.DeleteChecked = function () {
            var checkedDataList = jQuery.grep($scope.Employees, function (employee, i) {
                return employee.selected == true;
            });
            if (checkedDataList.length > 0) {
                //$.each(checkedDataList, function (i, e) {
                //    $scope.DeleteEmployee(e.Id);
                //})
                var checked = [];
                $.each(checkedDataList, function (i, e) {
                        checked.push(e.Id)
                })
                var csvchecked = [];
                csvchecked = checked.join(',');
                $scope.DeleteEmployee(csvchecked);
            }
        }

        ////For dropDown calling SkillTable
        function GetSkill() {
            skillServices.GetSkill().then(function (data) {
                $scope.GetSkill = data;
            })
        };
        GetSkill();

        GetEmployee();
        $scope.GetEmployee = {};
        $scope.CreateEmployee = {};
     
        //Crude Function
        function GetEmployee() {
            employeeServices.GetEmployee().then(function (data) {
                $scope.Employees = data;  //data is passed to UI
            })
        };

        GetEmployee();
        $scope.GetById = function (id) {
            employeeServices.GetById(id).then(function (data) {
                if (data != null) {
                    $scope.Employee = data;
                }
            })
        };

        $scope.AddEmployee = function (model) {
            employeeServices.AddEmployee(model).then(function (data) {  //addTask is passed to taskServices.
                if (data) {
                    $.notify({
                        title: "",
                        message: "Task Successfully Added!",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
                    GetEmployee();
                    $("#myModal").modal('hide');
                }
            })
        };

        $scope.UpdateEmployee = function (model) {
            employeeServices.UpdateEmployee(model).then(function (data) {
                if (data) {
                    $.notify({
                        title: "",
                        message: "Task Successfully Updated",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
               
                    GetEmployee();
                    
                    $("#myModal1").modal('hide');
                }
            })
        };

        $scope.DeleteEmployee = function (id) {
            var result = confirm("Are you sure want to delete?");
            if (result) {
                employeeServices.DeleteEmployee(id).then(function (data) {
                    if (data) {
                        $.notify({
                            title: "",
                            message: "Task Successfully Updated",
                            icon: 'fa fa-check'
                        }, {
                            type: "info"
                        });
                        GetEmployee();
                        $("#modal2").modal('hide');
                    }
                })
            }
        };
 
  
        //for checkbox of AddSkill PopUp Table
        $scope.skillselectall = false;
        var checked = [];
        $scope.skillselectcheckbox = function (skillselectall) {
            if (skillselectall) {
                $scope.GetSkill.forEach(function (data) {
                    data.selectedskill = true;
                })
            } else {
                $scope.GetSkill.forEach(function (data) {
                    data.selectedskill = false;
                })
            }
        };

        $scope.skillcheckuncheck = function () {
            var checkedDataList = jQuery.grep($scope.GetSkill, function (skill, i) {
                return skill.selectedskill == true;
            });
            var check = $scope.GetSkill.length;
            if (checkedDataList.length != check) {
                $scope.skillselectall = false;
            }
            else {
                $scope.skillselectall = true;
            }
        };

        $scope.CheckedSkillstring = function (ind, Emp) {
            //var checked = [];
            var checkedDataList = jQuery.grep($scope.GetSkill, function (skill, i) {
                return skill.selectedskill == true;
            });
            if (checkedDataList.length > 0) {
                $.each(checkedDataList, function (i, s) {
                    checked.push(s.SkillName)
                })

            }
            var csvchecked = [];
            csvchecked = checked.join(',');
            $scope.AddEmployeeSkill(Emp, csvchecked);

        }

        $scope.AddEmployeeSkill = function (Emp, skillname) {
            employeeServices.AddEmployeeSkill(Emp, skillname).then(function (data) {  //addTask is passed to taskServices.
                if (data) {
                    $.notify({
                        title: "",
                        message: "Task Successfully Added!",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
                    GetEmployee();
                    $("#myModal").modal('hide');
                }
            })
        };
     
        $scope.GetFilteredSkill = function (skillid, skill) {
            var GetFilteredSkill = [];
            ///var otherskill = csv.split(',');
            $.each(skill, function (i, s)
            {      
                    if (s.Id != skillid) {
                        GetFilteredSkill.push(s);
                    }   
            })
            $scope.GetFilterSkill = GetFilteredSkill;
        };
    }
})();