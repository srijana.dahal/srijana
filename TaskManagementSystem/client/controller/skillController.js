﻿(function () {
    'use strict';
    myApp.controller('skillController', skillController);
    skillController.$inject = ['$scope', '$location', 'skillServices', '$filter'];

    function skillController($scope, $location, skillServices, $filter) {


        GetSkill();
        $scope.Skill = {};
        $scope.CreateSkill = {};

        function GetSkill() {
            skillServices.GetSkill().then(function (data) {
                $scope.Skills = data;  

            })
        };

        GetSkill();
        $scope.GetById = function (id) {
            skillServices.GetById(id).then(function (data) {
                if (data != null) {
                    $scope.Skill = data;
                }
            })
        };

        $scope.AddSkill = function (model) {
            skillServices.AddSkill(model).then(function (data) {
                if (data) {
                    $.notify({
                        title: "",
                        message: "Skill Successfully Added!",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
                    GetSkill();
                    $("#myModal").modal('hide');
                }
            })
        };

        $scope.UpdateSkill = function (model) {
            skillServices.UpdateSkill(model).then(function (data) {
                if (data) {
                    $.notify({
                        title: "",
                        message: "Skill Successfully Updated",
                        icon: 'fa fa-check'
                    }, {
                        type: "info"
                    });
                    GetSkill();
                    $("#myModal1").modal('hide');
                }
            })
        };

        //$scope.DeleteSkill = function (id) {
        //    var result = confirm("Are you sure want to delete?");
        //    if (result) {
        //        skillServices.DeleteSkill(id).then(function (data) {
        //            if (data) {
        //                $.notify({
        //                    title: "",
        //                    message: "Skill Successfully Deleted",
        //                    icon: 'fa fa-check'
        //                }, {
        //                    type: "info"
        //                });
        //                GetSkill();
        //                $("#modal2").modal('hide');
        //            }
        //        })
        //    }

        //};


    }
})();