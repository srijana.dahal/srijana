﻿var myApp = angular.module("myApp", ["ui.router", "ui.bootstrap"]);
myApp.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
         .state('home', {
             url: '/home',
             templateUrl: '/client/views/home.html'

         })
         .state('employee', {
             url: '/employee',
             templateUrl: '/client/views/employee.html',
             controller: 'employeeCtrl',
             controllerAs: 'employeeCtrl'

         })
            .state('tasks', {
                url: '/task',
                templateUrl: '/client/views/task.html',
                controller: 'taskController',
                controllerAs: 'taskController'

            })
                .state('skills', {
                    url: '/skill',
                    templateUrl: '/client/views/skill.html',
                    controller: 'skillController',
                    controllerAs: 'skillController'
                })
   

}]);

//pagination

myApp.constant('serviceBasePath', 'http://localhost:22151');

