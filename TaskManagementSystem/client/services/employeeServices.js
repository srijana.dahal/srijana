﻿(function () {
    'use strict'
    myApp.factory('employeeServices', employeeServices);
    employeeServices.$inject = ['$http', 'serviceBasePath', '$q'];

    function employeeServices($http, serviceBasePath, $q) {
        var fac = {};

        fac.GetEmployee = function () {
            var config = { headers: { 'Content-Type': 'application/json' } };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/employee/GetEmployee', config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise
        }

        fac.GetById = function (id) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/employee/GetById', { params: { id: id } }, config).success(function (response) {     //1st id=employeecontroller,2nd id = from function(id)
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }

        fac.AddEmployee = function (model) {
            var config = { headers: { 'Content-Type': 'application/json' } };

            var data = {
                FirstName: model.FirstName,
                LastName: model.LastName,
                Email: model.Email,
                SkillId: model.SkillId,
                Address: model.Address,
                PhoneNumber: model.PhoneNumber,
                AddedDate: model.AddedDate,
                Status: model.Status,
            }
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/employee/addemployees', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;
        }

        fac.DeleteEmployee = function (EmpIDs)
        {
            var config = {
                 headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/employee/deleteemployee', { params: { EmpIDs: EmpIDs } }, config).success(function (response) {
                     deferred.resolve(response);
                 }).error(function (err) {
                      console.log(err);
                        deferred.reject(err);
                       });
                    return deferred.promise;
        }
   
        fac.UpdateEmployee = function (model) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/employee/updateemployee', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return deferred.promise;

        }

        fac.AddEmployeeSkill = function (model,skillname) {
            var config = { headers: { 'Content-Type': 'application/json' } };

            var data = {
                id: model.Id,
                FirstName: model.FirstName,
                LastName: model.LastName,
                Email: model.Email,
                SkillId:model.SkillId,
                OtherSkillName: skillname,
                Address: model.Address,
                PhoneNumber: model.PhoneNumber,
                AddedDate: model.AddedDate,
                Status: model.Status,
            }
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/employee/updateemployee', data, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;
        }

        
        return fac;
    }
})();

