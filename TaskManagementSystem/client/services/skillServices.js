﻿(function () {
    'use strict'
    myApp.factory('skillServices', skillServices);
    skillServices.$inject = ['$http', 'serviceBasePath', '$q'];

    function skillServices($http, serviceBasePath, $q) {
        var fac = {};

        fac.GetSkill = function ()
        {
            var config = { headers: {'Content-Type': 'application/json'}};
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/skill/GetSkill', config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise
        }

        fac.GetById = function (id) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/skill/GetById', { params: { id: id } }, config).success(function (response) {     //1st id=employeecontroller,2nd id = from function(id)
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }

        fac.AddSkill = function (model) {
            var config = { headers: { 'Content-Type': 'application/json' } };

            var data = {
                SkillName: model.SkillName,
            }
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/skill/addskills', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;
        }
        //fac.DeleteSkill = function (id) {

        //    var config = {
        //        headers: { 'Content-Type': 'application/json' }
        //    };
        //    var deferred = $q.defer();
        //    $http.get(serviceBasePath + '/api/task/deleteskill', { params: { id: id } }, config).success(function (response) {
        //        deferred.resolve(response);
        //    }).error(function (err) {
        //        console.log(err);
        //        deferred.reject(err);
        //    });

        //    return deferred.promise;

        //}

        fac.UpdateSkill = function (model) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/skill/updateskill', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return deferred.promise;

        }


         return fac;
    }
})();

