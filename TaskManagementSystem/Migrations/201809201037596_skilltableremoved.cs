namespace TaskManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class skilltableremoved : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.EmployeeSkills");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeSkills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        SkillId = c.String(),
                        SkillName = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        NoOfEmp = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
