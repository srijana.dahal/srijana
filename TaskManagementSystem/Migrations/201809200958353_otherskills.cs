namespace TaskManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class otherskills : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "SkillName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "SkillName");
        }
    }
}
