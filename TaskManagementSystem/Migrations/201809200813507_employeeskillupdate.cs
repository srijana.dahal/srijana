namespace TaskManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeeskillupdate : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EmployeeSkillViewModels", newName: "EmployeeSkills");
            AlterColumn("dbo.EmployeeSkills", "SkillId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EmployeeSkills", "SkillId", c => c.Int(nullable: false));
            RenameTable(name: "dbo.EmployeeSkills", newName: "EmployeeSkillViewModels");
        }
    }
}
