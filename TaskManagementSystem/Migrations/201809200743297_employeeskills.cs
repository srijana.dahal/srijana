namespace TaskManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeeskills : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeSkillViewModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        SkillId = c.Int(nullable: false),
                        SkillName = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(nullable: false),
                        AddedDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        NoOfEmp = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmployeeSkillViewModels");
        }
    }
}
