namespace TaskManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class otherskillname : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "OtherSkillName", c => c.String());
            DropColumn("dbo.Employees", "SkillName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "SkillName", c => c.String());
            DropColumn("dbo.Employees", "OtherSkillName");
        }
    }
}
