﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementSystem.Services;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Controllers
{
    public class SkillController : ApiController
    {
        private SkillServices _skillservices = new SkillServices();
        protected DataContext db = new DataContext();

        [HttpGet]
        [Route("api/skill/GetSkill")]
        public IHttpActionResult GetSkills()
        { 
            //var data = "srijana";
            var data = _skillservices.GetSkill();
            return Ok(data);
        }

        [HttpGet]
        [Route("api/skill/GetById")]
        public IHttpActionResult GetById(int id)
        {
            var data = _skillservices.GetSkill(id);
            return Ok(data);
        }

        [HttpPost]
        [Route("api/skill/addskills")]
        public IHttpActionResult AddSkill([FromBody] SkillViewModel Model)
        {
            var result = _skillservices.AddSkill(Model);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/skill/updateskill")]
        public IHttpActionResult UpdateSkill([FromBody]SkillViewModel Model)
        {
            var data = _skillservices.UpdateSkill(Model);
            return Ok(data);
        }
        //[HttpGet]
        //[Route("api/task/deleteskill")]
        //public IHttpActionResult DeleteSkill(int? id)
        //{
        //    var data = _skillservices.DeleteSkill(id ?? 0);
        //    return Ok(data);
        //}
     
    }
}
