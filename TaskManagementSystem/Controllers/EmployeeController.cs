﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementSystem.Services;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Controllers
{
    public class EmployeeController : ApiController
    { 

        private EmployeeServices _employeeservices = new EmployeeServices();
        protected DataContext db = new DataContext();

        [HttpGet]
        [Route("api/employee/GetEmployee")]
        public IHttpActionResult GetEmployee()
        {
            var data = _employeeservices.GetEmployee();
            return Ok(data);
        }

        [HttpGet]
        [Route("api/employee/GetById")]
        public IHttpActionResult GetById(int id)
        {
            var data = _employeeservices.GetEmployee(id);
            return Ok(data);
        }

        [HttpPost]
        [Route("api/employee/addemployees")]
        public IHttpActionResult AddEmployee([FromBody]EmployeeViewModel Model)
        {
            var result = _employeeservices.AddEmployee(Model);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/employee/updateemployee")]
        public IHttpActionResult UpdateEmployee([FromBody]EmployeeViewModel Model)
        {
            var data = _employeeservices.UpdateEmployee(Model);
            return Ok(data);
        }

        [HttpGet]
        [Route("api/employee/deleteemployee")]
        public IHttpActionResult DeleteEmployee(string EmpIDs)
        {
            var data = _employeeservices.DeleteEmployee(EmpIDs);
            return Ok(data);
        }

       
    }
}
