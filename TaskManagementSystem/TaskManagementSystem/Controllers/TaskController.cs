﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementSystem.Services;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Controllers
{
    public class TaskController : ApiController
    {
        private TaskServices _taskservices = new TaskServices();
        protected DataContext db = new DataContext();
        //public TaskController()
        //{

        //}
        //public TaskController(ITaskServices taskservices)
        //{
        //    _taskservices = taskservices;
        //}
        [HttpPost]
        [Route("api/task/addtasks")]
        public IHttpActionResult AddTask([FromBody]TaskViewModel Model)
        {
            var result = _taskservices.AddTask(Model);
            return Ok(result);
        }
        [HttpPost]
        [Route("api/task/updatetask")]
        public IHttpActionResult UpdateTask([FromBody]TaskViewModel Model)
        {
            var data = _taskservices.UpdateTask(Model);
            return Ok(data);
        }
        [HttpGet]
        [Route("api/task/deteletask")]
        public IHttpActionResult DeleteTask(int? id)
        {
            var data = _taskservices.DeleteTask(id ?? 0);
            return Ok(data);
        }
        [HttpGet]
        [Route("api/task/GetTasks")]
        public IHttpActionResult GetTasks()
        {
            //var data = "srijana";
            var data = _taskservices.GetTask();
            return Ok(data);
            //var query = db.Tasks.Select(x => new TaskViewModel()
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    Description = x.Description,
            //    Status = x.Status
            //}).ToList();

            //return Ok(query);
        }
        [HttpGet]
        [Route("api/task/GetById")]
        public IHttpActionResult GetById(int id)
        {
            var data = _taskservices.GetTask(id);
            return Ok(data);
        }

    }
}
