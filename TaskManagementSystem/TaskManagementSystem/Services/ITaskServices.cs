﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskManagementSystem.Tables;

namespace TaskManagementSystem.Services
{
    public interface ITaskServices
    {

        bool AddTask(TaskViewModel Model);
        IEnumerable<TaskViewModel> GetTask();
        bool UpdateTask(TaskViewModel Model);
        bool DeleteTask(int Id);
        TaskViewModel GetTask(int id);

    }
}