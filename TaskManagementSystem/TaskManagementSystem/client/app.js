﻿var myApp = angular.module("myApp", ["ui.router"]);
myApp.config(function ($stateProvider) {
    $stateProvider
.state('tasks', {
    url: '/task',
    templateUrl: '/client/views/task.html',
    controller: 'taskController',
    controllerAs: 'taskController'

})
});


myApp.constant('serviceBasePath', 'http://localhost:22151');
