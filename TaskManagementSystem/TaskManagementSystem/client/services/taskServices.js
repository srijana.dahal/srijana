﻿(function () {
    'use strict';

    myApp.factory('taskServices', taskServices);

    taskServices.$inject = ['$http', 'serviceBasePath', '$q'];

    function taskServices($http, serviceBasePath, $q) {
        var fac = {};

        fac.GetTask = function ()
        {
            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/task/GetTasks', config).success(function (response) {     //1st id=employeecontroller,2nd id = from function(id)
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;
           
        }

        fac.GetById = function (id)
        {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/task/GetById', { params: { id: id } }, config).success(function (response) {     //1st id=employeecontroller,2nd id = from function(id)
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;

        }

        fac.AddTask = function (model)
        {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var data = {
                Name: model.Name,
                Description: model.Description,
                AddedDate:model.AddedDate,
                Status: model.Status
            }
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/task/addtasks', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return deferred.promise;

        }

        fac.DeleteTask = function (id) 
        {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.get(serviceBasePath + '/api/task/deteletask', { params:{id: id}}, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return deferred.promise;

        }

        fac.UpdateTask = function (model) {

            var config = {
                headers: { 'Content-Type': 'application/json' }
            };
            var deferred = $q.defer();
            $http.post(serviceBasePath + '/api/task/updatetask', model, config).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return deferred.promise;

        }

        return fac;
    }
})();